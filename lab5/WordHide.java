//*********************************
// Honor Code: This work I am submitting is a result of my own thinking and efforts.
// David Nadel and Vance Murphy
// CMPSC 111 Spring 2017
// 23 February 2017
// Lab #5
//
// Purpose: To practice hiding messages inside of non-secret content usinh steganography.
// WordHide.java
//*********************************

import java.util.Scanner;
import java.util.Date;
import java.util.Random;


public class WordHide{ 
    public static void main(String[] args){
		// scanner
		Scanner scan = new Scanner(System.in);
		Random rand = new Random();

		// variables
		String name;
		String padding;
		String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		boolean placed = false;
		char l1, l2, l3, l4, l5;
	

		System.out.println("David Nadel, Vance Murphy " + new Date());
		System.out.println("Lab #5");

		System.out.println("Enter a 10 letter word (if it is not 10 letters, add padding characters."); // prompts the user to enter a 10 letter word and add padding characters if not 10 letters
		name = scan.next(); // stores the word
		name = name.toUpperCase(); // converts the 10 letter word to all upper case letters

		l1 = alpha.charAt(rand.nextInt(26)); // chooses a random character from the alphabet
		l2 = alpha.charAt(rand.nextInt(26)); // chooses a random character from the alphabet
		l3 = alpha.charAt(rand.nextInt(26)); // chooses a random character from the alphabet
		l4 = alpha.charAt(rand.nextInt(26)); // chooses a random character from the alphabet
		l5 = alpha.charAt(rand.nextInt(26)); // chooses a random character from the alphabet
		System.out.print(l1+" "+name.charAt(0)+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "); // prints out 20 letters horizontally with one letter of the word hidden inside
		System.out.println(); // code repeats 19 more times (see above)
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+name.charAt(1)+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+name.charAt(2)+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+name.charAt(3)+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+name.charAt(4)+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+name.charAt(5)+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+name.charAt(6)+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+name.charAt(7)+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+name.charAt(8)+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+name.charAt(9)+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();
		
		l1 = alpha.charAt(rand.nextInt(26));
		l2 = alpha.charAt(rand.nextInt(26));
		l3 = alpha.charAt(rand.nextInt(26));
		l4 = alpha.charAt(rand.nextInt(26));
		l5 = alpha.charAt(rand.nextInt(26));
		System.out.print(l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" "+l1+" "+l2+" "+l3+" "+l4+" "+l5+" ");
		System.out.println();



















		

	
	}
}
